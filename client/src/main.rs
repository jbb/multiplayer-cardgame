use macroquad::audio::Sound;
use std::collections::HashMap;
use std::error::Error;
use std::io::BufReader;
use std::io::Read;
use std::net::TcpStream;
use std::time::Duration;

use ::rand::Rng; // 0.8

use cardscommon::{send_message, Card, Message, Jeton, cards_value, ProfitDistribution};

use macroquad::audio::{load_sound, play_sound, PlaySoundParams};
use macroquad::prelude::*;

const CARD_TEXTURE_WIDTH: f32 = 242.;
const CARD_TEXTURE_HEIGHT: f32 = 336.;

const JETON_TEXTURE_WIDTH: f32 = 378.;

const PROPERTIES: [&str; 13] = ["fast", "slow", "stupid", "annoying", "random", "best", "huge", "fat", "small", "funny", "flying", "winning", "loosing"];
const ANIMALS: [&str; 16] = ["Turtle", "Monkey", "Cat", "Dog", "Horse", "Elephant", "Donkey", "Bug", "Duck", "Pig", "Sheep", "Human", "Chicken", "Fish", "Creeper", "Bee"];

fn generate_user_name() -> String {
    let mut rng = ::rand::thread_rng();
    let property = PROPERTIES[rng.gen_range(0..PROPERTIES.len())];
    let animal = ANIMALS[rng.gen_range(0..ANIMALS.len())];
    let number = rng.gen_range(10..99);
    
    format!("{}{}{}", property, animal, number)
}

struct NetworkClient {
    stream: TcpStream,
}

impl NetworkClient {
    fn new(address: &str) -> std::io::Result<NetworkClient> {
        let conn = TcpStream::connect(address)?;
        conn.set_read_timeout(Some(Duration::from_millis(90)))?;

        Ok(NetworkClient { stream: conn })
    }

    async fn process_event(&mut self) -> Result<(Vec<Message>, &mut TcpStream), Box<dyn Error>> {
        let mut line = String::new();
        match BufReader::new(&self.stream).read_to_string(&mut line) {
            Ok(_) => {}
            Err(t) if t.kind() == std::io::ErrorKind::WouldBlock => {
                //eprintln!("Would block");
            }
            Err(e) => eprintln!("{:?}", e),
        }
        //println!("Line: {}", line);

        let msgs = line
            .split('\n')
            .filter_map(|msg| {
                if !msg.is_empty() {
                    let message: Option<Message> = serde_json::from_str(&msg).ok();
                    return message;
                }

                return None;
            })
            .collect::<Vec<Message>>();

        return Ok((msgs, &mut self.stream));
    }
}

#[derive(Debug)]
struct PlayerState {
    nick: String,
    stake: Option<Jeton>,
    cards: Vec<Card>,
    round_over: bool,
    profit: Option<ProfitDistribution>
}

impl PlayerState {
    fn new(nick: String) -> PlayerState {
        PlayerState {
            nick,
            stake: None,
            cards: Vec::new(),
            round_over: false,
            profit: None
        }
    }
}

#[derive(Debug, PartialEq)]
enum Status {
    Waiting,
    SetStake,
    Action,
    Results
}

struct Round {
    players: HashMap<String, PlayerState>,
    dealer_cards: Vec<Card>,
    status: Status
}

impl Round {
    pub fn new() -> Round {
        Round {
            players: HashMap::new(),
            dealer_cards: Vec::new(),
            status: Status::Waiting
        }
    }
}

fn card_texture_coordinates(card: &Card) -> (f32, f32) {
    match card {
        Card::ClubsAce => (0f32, 0f32),
        Card::ClubsTwo => (CARD_TEXTURE_WIDTH, 0f32),
        Card::ClubsThree => (CARD_TEXTURE_WIDTH * 2.0, 0f32),
        Card::ClubsFour => (CARD_TEXTURE_WIDTH * 3.0, 0f32),
        Card::ClubsFive => (CARD_TEXTURE_WIDTH * 4.0, 0f32),
        Card::ClubsSix => (CARD_TEXTURE_WIDTH * 5.0, 0f32),
        Card::ClubsSeven => (CARD_TEXTURE_WIDTH * 6.0, 0f32),
        Card::ClubsEight => (CARD_TEXTURE_WIDTH * 7.0, 0f32),
        Card::ClubsNine => (CARD_TEXTURE_WIDTH * 8.0, 0f32),
        Card::ClubsTen => (CARD_TEXTURE_WIDTH * 9.0, 0f32),
        Card::ClubsJack => (CARD_TEXTURE_WIDTH * 10.0, 0f32),
        Card::ClubsQueen => (CARD_TEXTURE_WIDTH * 11.0, 0f32),
        Card::ClubsKing => (CARD_TEXTURE_WIDTH * 12.0, 0f32),
        Card::DiamondsAce => (0f32, CARD_TEXTURE_HEIGHT),
        Card::DiamondsTwo => (CARD_TEXTURE_WIDTH, CARD_TEXTURE_HEIGHT),
        Card::DiamondsThree => (CARD_TEXTURE_WIDTH * 2.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsFour => (CARD_TEXTURE_WIDTH * 3.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsFive => (CARD_TEXTURE_WIDTH * 4.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsSix => (CARD_TEXTURE_WIDTH * 5.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsSeven => (CARD_TEXTURE_WIDTH * 6.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsEight => (CARD_TEXTURE_WIDTH * 7.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsNine => (CARD_TEXTURE_WIDTH * 8.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsTen => (CARD_TEXTURE_WIDTH * 9.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsJack => (CARD_TEXTURE_WIDTH * 10.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsQueen => (CARD_TEXTURE_WIDTH * 11.0, CARD_TEXTURE_HEIGHT),
        Card::DiamondsKing => (CARD_TEXTURE_WIDTH * 12.0, CARD_TEXTURE_HEIGHT),
        Card::HeartsAce => (0f32, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsTwo => (CARD_TEXTURE_WIDTH, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsThree => (CARD_TEXTURE_WIDTH * 2.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsFour => (CARD_TEXTURE_WIDTH * 3.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsFive => (CARD_TEXTURE_WIDTH * 4.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsSix => (CARD_TEXTURE_WIDTH * 5.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsSeven => (CARD_TEXTURE_WIDTH * 6.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsEight => (CARD_TEXTURE_WIDTH * 7.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsNine => (CARD_TEXTURE_WIDTH * 8.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsTen => (CARD_TEXTURE_WIDTH * 9.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsJack => (CARD_TEXTURE_WIDTH * 10.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsQueen => (CARD_TEXTURE_WIDTH * 11.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::HeartsKing => (CARD_TEXTURE_WIDTH * 12.0, CARD_TEXTURE_HEIGHT * 2.0),
        Card::SpadesAce => (0f32, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesTwo => (CARD_TEXTURE_WIDTH, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesThree => (CARD_TEXTURE_WIDTH * 2.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesFour => (CARD_TEXTURE_WIDTH * 3.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesFive => (CARD_TEXTURE_WIDTH * 4.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesSix => (CARD_TEXTURE_WIDTH * 5.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesSeven => (CARD_TEXTURE_WIDTH * 6.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesEight => (CARD_TEXTURE_WIDTH * 7.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesNine => (CARD_TEXTURE_WIDTH * 8.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesTen => (CARD_TEXTURE_WIDTH * 9.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesJack => (CARD_TEXTURE_WIDTH * 10.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesQueen => (CARD_TEXTURE_WIDTH * 11.0, CARD_TEXTURE_HEIGHT * 3.0),
        Card::SpadesKing => (CARD_TEXTURE_WIDTH * 12.0, CARD_TEXTURE_HEIGHT * 3.0),
    }
}

fn jeton_texture_coordinates(jeton: &Jeton) -> (f32, f32) {
    match jeton {
        Jeton::One => (0f32, 0f32),
        Jeton::TwoPointFive => (JETON_TEXTURE_WIDTH, 0f32),
        Jeton::Five => (JETON_TEXTURE_WIDTH * 2., 0f32),
        Jeton::Ten => (JETON_TEXTURE_WIDTH * 3., 0f32),
        Jeton::Twenty => (JETON_TEXTURE_WIDTH * 4., 0f32),
        Jeton::TwentyFive => (JETON_TEXTURE_WIDTH * 5., 0f32),
        Jeton::Hundred => (JETON_TEXTURE_WIDTH * 6., 0f32),
        Jeton::FiveHundred => (JETON_TEXTURE_WIDTH * 7., 0f32)
    }
}

fn status_text(round: &Round, player: &PlayerState) -> String {
    let status: String = {
        if let Some(profit) = player.profit {
            match profit {
                ProfitDistribution::Loss => "lost".to_string(),
                ProfitDistribution::Tie => "tie".to_string(),
                ProfitDistribution::Win => "won".to_string()
            }
        } else if player.round_over {
            "Exceeded 21".to_string()
        } else {
            format!("{:?}", round.status)
        }
    };

    format!("{} ({}): {}", player.nick, status, cards_value(&player.cards))
}


struct Window<'w> {
    // TEXTURES:
    bg_home: Texture2D,
    bg_game: Texture2D,
    cards: Texture2D,
    card_back: Texture2D,
    jetons: Texture2D,
    // FONTS:
    font: Font,
    // SOUNDS:
    sound_card: Sound,
    sound_jeton: Sound,
    // VARS:
    background: Texture2D,
    started: bool,
    ready: bool,
    show_controls: bool,
    controls: [&'w str; 10],

    //
    players: Vec<String>,
    current_round: Option<Round>,
    own_name: Option<String>,
    
    //
    game_end_timer: Option<i32>
}

impl<'w> Window<'w> {
    async fn new() -> Result<Window<'w>, Box<dyn Error>> {
        Ok(Window {
            bg_home: load_texture("client/resources/textures/bgHome.png").await?,
            bg_game: load_texture("client/resources/textures/bgGame.png").await?,
            cards: load_texture("client/resources/textures/card_atlas.png").await?,
            card_back: load_texture("client/resources/textures/card_back.png").await?,
            jetons: load_texture("client/resources/textures/jeton_atlas.png").await?,
            font: load_ttf_font("client/resources/fonts/NotoSans-Regular.ttf").await?,
            sound_card: load_sound("client/resources/sounds/placeCard.wav").await?,
            sound_jeton: load_sound("client/resources/sounds/placeJeton.wav").await?,
            background: load_texture("client/resources/textures/bgHome.png").await?,
            started: false,
            ready: false,
            show_controls: false,
            controls: [
                "[1] Bet 1",
                "[2] Bet 2.5",
                "[3] Bet 5",
                "[4] Bet 10",
                "[5] Bet 20",
                "[6] Bet 25",
                "[7] Bet 100",
                "[8] Bet 500",
                "[ENTER] Hit",
                "[SPACE] Stand",
            ],
            current_round: None,
            players: Vec::new(),
            own_name: None,
            game_end_timer: None
        })
    }

    async fn process_event(
        &mut self,
        message: Option<Message>,
        mut stream: Option<&mut TcpStream>,
    ) -> Result<(), Box<dyn Error>> {
        if let Some(ref msg) = message {
            println!("<<< {:?}", msg);
        }
        match message {
            Some(message) => {
                match message {
                    Message::Hello(text) => {
                        println!("Server says: {}", text);
                        send_message(
                            stream.expect("Seems this is possible"),
                            Message::Hello("Cool!".to_string()),
                        )?;
                        return Ok(());
                    }
                    Message::Joined => {
                        println!("Successfully joined the game");
                        return Ok(());
                    }
                    Message::Error(err) => {
                        println!("Error: {:?}", err);
                        return Ok(());
                    }
                    Message::UserJoined(name) => {
                        println!("{} joined the game", name.clone());
                        self.players.push(name);
                        return Ok(());
                    }
                    Message::GameStarted => {
                        println!("Game started");
                        self.current_round = Some(Round::new());
                        self.current_round.as_mut().expect("Round should be started by now").status = Status::SetStake;

                        // Create state for each player that lives for the time of one round
                        for player in &self.players {
                            self.current_round
                                .as_mut()
                                .expect("We just created the round one line above")
                                .players
                                .insert(player.clone(), PlayerState::new(player.clone()));
                        }

                        self.current_round
                            .as_mut()
                            .expect("Round should exist at this point")
                            .players
                            .insert(
                                self.own_name
                                    .as_ref()
                                    .expect("Name should be known here")
                                    .clone(),
                                PlayerState::new(
                                    self.own_name
                                        .as_ref()
                                        .expect("Name should be known here")
                                        .clone(),
                                ),
                            );
                    }
                    Message::UserSetStake(name, stake) => {
                        println!("{} set stake to {:?}", name, stake);
                        self.current_round
                            .as_mut()
                            .expect("Round should exist at this point")
                            .players
                            .get_mut(&name)
                            .expect("Users should be known when this is called")
                            .stake = Some(stake);
                    }
                    Message::DealCards(name, mut cards) => {
                        eprintln!("{} got cards: {:?}", name, cards);
                        self.current_round.as_mut().expect("Round should be started by now").status = Status::Action;
                        self.current_round
                            .as_mut()
                            .expect("Round should exist at this point")
                            .players
                            .get_mut(&name)
                            .expect("Users should be known when this is called")
                            .cards
                            .append(&mut cards);
                    }
                    Message::ShowDealerCard(card) => {
                        self.current_round
                            .as_mut()
                            .expect("This should only happen while the game is running")
                            .dealer_cards
                            .push(card);

                        println!(
                            "Dealer cards are now: {:?}",
                            self.current_round
                                .as_ref()
                                .expect("This should only happen while the game is running")
                                .dealer_cards
                        );
                    }
                    Message::Over21(nick) => {
                        println!("Got over 21");
                        self.current_round
                            .as_mut()
                            .expect("Round should exist at this point")
                            .players
                            .get_mut(&nick)
                            .expect("Own player should exist in the game")
                            .round_over = true;
                    }
                    Message::ProfitDistribution(nick, outcome) => {
                        self.current_round.as_mut().expect("Round should be started by now").status = Status::Results;
                        println!("Round outcome for player: {} {:?}", nick, outcome);

                        if let Some(ref mut player) = self.current_round
                            .as_mut()
                            .expect("Round should exist at this point")
                            .players
                            .get_mut(&nick) {
                            player.profit = Some(outcome);
                        }
                    }
                    Message::GameEnded => {
                        self.game_end_timer = Some(0);
                    }
                    _ => {
                        panic!("Unsupported message received");
                    }
                }
            }
            None => {}
        }
        
        if let Some(ref mut time) = self.game_end_timer {
            if *time >= 100 {
                self.current_round = None;
                self.game_end_timer = None;
                self.started = false;
                self.ready = false;
                self.show_controls = false;

                self.background = self.bg_home;

                // Not sure
                self.players.clear();
            } else {
                *time += 1
            }
        }

        clear_background(WHITE);
        draw_texture_ex(
            self.background,
            0.0,
            0.0,
            WHITE,
            DrawTextureParams {
                dest_size: Some(vec2(screen_width(), screen_height())),
                ..Default::default()
            },
        );

        if !self.started {
            // HOME SCREEN:
            draw_rectangle(0., screen_height() / 2.5, screen_width(), screen_height() / 7., Color {r: 0., g: 0., b: 0., a: 0.8});

            draw_text_ex("Press [ENTER] to search for a game", screen_width() / 3.5, screen_height() / 2.05,
                TextParams {
                    font_size: (screen_height() + screen_width()) as u16 / 60,
                    //font_scale: get_time().sin() as f32 / 32. + 1.,
                    font: self.font,
                    ..Default::default()
                }
            );
        } else {
            // GAME SCREEN:
            if !self.ready {
                draw_rectangle(0., screen_height() / 2.5, screen_width(), screen_height() / 7., Color {r: 0., g: 0., b: 0., a: 0.8});

                draw_text_ex("Press [ENTER] when ready", screen_width() / 3., screen_height() / 2.05,
                    TextParams {
                        font_size: (screen_height() + screen_width()) as u16 / 60,
                        font: self.font,
                        ..Default::default()
                    }
                );
            } else {
                // CONTROLS:
                draw_text_ex(
                    "[H] Controls",
                    25.0,
                    45.0,
                    TextParams {
                        font_size: 30,
                        font: self.font,
                        color: GRAY,
                        ..Default::default()
                    },
                );
            }

            if self.show_controls {
                for i in 0..=self.controls.len() - 1 {
                    draw_text_ex(
                        self.controls[i],
                        25.0,
                        45.0 + ((i + 1) * 25) as f32,
                        TextParams {
                            font_size: 20,
                            font: self.font,
                            color: GRAY,
                            ..Default::default()
                        },
                    );
                }
            }

            // DEALER:
            if let Some(ref round) = self.current_round {
                //println!("Dealer Cards: {:?}", round.dealer_cards);
                if round.dealer_cards.len() == 1 {
                    draw_texture_ex(
                        self.card_back,
                        screen_width() / 2.72 + ((2) as f32 * screen_width() / 30.),
                        screen_height() / 4.,
                        WHITE,
                        DrawTextureParams {
                            dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                            rotation: 0.,
                            ..Default::default()
                        },
                    );
                }

                for i in 0..round.dealer_cards.len() {
                    let (x, y) = card_texture_coordinates(&round.dealer_cards[i]);
                    draw_texture_ex(
                        self.cards,
                        screen_width() / 2.72 + ((i + 1) as f32 * screen_width() / 30.),
                        screen_height() / 4.,
                        WHITE,
                        DrawTextureParams {
                            source: Some(Rect {
                                x,
                                y,
                                w: CARD_TEXTURE_WIDTH,
                                h: CARD_TEXTURE_HEIGHT,
                            }),
                            dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                            rotation: 0.,
                            ..Default::default()
                        },
                    );
                }
            }

            // PLAYER 1:
            if let Some(ref round) = self.current_round {
                if let Some(name) = self.players.get(0) {
                    if let Some(ref player) = round.players.get(name) {
                        for i in 0..player.cards.len() {
                            let (x, y) = card_texture_coordinates(&player.cards[i]);
                            draw_texture_ex(
                                self.cards,
                                screen_width() / 2.625 + (i as f32 * screen_width() / 30.),
                                screen_height() / 1.6,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x: x,
                                        y: y,
                                        w: CARD_TEXTURE_WIDTH,
                                        h: CARD_TEXTURE_HEIGHT,
                                    }),
                                    dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                                    rotation: 0.35,
                                    ..Default::default()
                                },
                            );
                        }

                        if let Some(stake) = player.stake {
                            let (x, y) = jeton_texture_coordinates(&stake);
                            draw_texture_ex(
                                self.jetons,
                                screen_width() / 2.74,
                                screen_height() / 1.35,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x,
                                        y,
                                        w: JETON_TEXTURE_WIDTH,
                                        h: JETON_TEXTURE_WIDTH
                                    }),
                                    dest_size: Some(vec2(screen_width() / 38.4, screen_height() / 21.6)),
                                    rotation: 0.35,
                                    ..Default::default()
                                },
                            );
                        }

                        draw_text_ex(
                            &status_text(round, player),
                            screen_width() / 2.5,
                            screen_height() / 1.27,
                            TextParams {
                                font_size: (screen_height() + screen_width()) as u16 / 100,
                                font: self.font,
                                color: MAGENTA,
                                ..Default::default()
                            },
                        );
                    }
                }
            }

            // PLAYER 2:
            if let Some(ref round) = self.current_round {
                if let Some(name) = self.players.get(1) {
                    if let Some(ref player) = round.players.get(name) {
                        for i in 0..player.cards.len() {
                            let (x, y) = card_texture_coordinates(&player.cards[i]);
                            draw_texture_ex(
                                self.cards,
                                screen_width() / 1.705 + (i as f32 * screen_width() / 30.),
                                screen_height() / 1.6,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x: x,
                                        y: y,
                                        w: CARD_TEXTURE_WIDTH,
                                        h: CARD_TEXTURE_HEIGHT,
                                    }),
                                    dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                                    rotation: -0.35,
                                    ..Default::default()
                                },
                            );
                        }

                        if let Some(stake) = player.stake {
                            let (x, y) = jeton_texture_coordinates(&stake);
                            draw_texture_ex(
                                self.jetons,
                                screen_width() / 1.64,
                                screen_height() / 1.35,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x,
                                        y,
                                        w: JETON_TEXTURE_WIDTH,
                                        h: JETON_TEXTURE_WIDTH
                                    }),
                                    dest_size: Some(vec2(screen_width() / 38.4, screen_height() / 21.6)),
                                    rotation: -0.35,
                                    ..Default::default()
                                },
                            );
                        }

                        draw_text_ex(
                            &status_text(round, player),
                            screen_width() / 1.53,
                            screen_height() / 1.27,
                            TextParams {
                                font_size: (screen_height() + screen_width()) as u16 / 100,
                                font: self.font,
                                color: MAGENTA,
                                ..Default::default()
                            },
                        );
                    }
                }
            }

            // PLAYER 3:
            if let Some(ref round) = self.current_round {
                if let Some(name) = self.players.get(2) {
                    if let Some(ref player) = round.players.get(name) {
                        for i in 0..player.cards.len() {
                            let (x, y) = card_texture_coordinates(&player.cards[i]);
                            draw_texture_ex(
                                self.cards,
                                screen_width() / 4. + (i as f32 * screen_width() / 30.),
                                screen_height() / 2.25,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x: x,
                                        y: y,
                                        w: CARD_TEXTURE_WIDTH,
                                        h: CARD_TEXTURE_HEIGHT,
                                    }),
                                    dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                                    rotation: 0.97,
                                    ..Default::default()
                                },
                            );
                        }

                        if let Some(stake) = player.stake {
                            let (x, y) = jeton_texture_coordinates(&stake);
                            draw_texture_ex(
                                self.jetons,
                                screen_width() / 4.98,
                                screen_height() / 1.9,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x,
                                        y,
                                        w: JETON_TEXTURE_WIDTH,
                                        h: JETON_TEXTURE_WIDTH
                                    }),
                                    dest_size: Some(vec2(screen_width() / 38.4, screen_height() / 21.6)),
                                    rotation: 0.97,
                                    ..Default::default()
                                },
                            );
                        }
                        
                        draw_text_ex(
                            &status_text(&round, player),
                            screen_width() / 4.15,
                            screen_height() / 1.7,
                            TextParams {
                                font_size: (screen_height() + screen_width()) as u16 / 100,
                                font: self.font,
                                color: MAGENTA,
                                ..Default::default()
                            },
                        );
                    }
                }
            }

            // PLAYER 4:
            if let Some(ref round) = self.current_round {
                if let Some(name) = self.players.get(3) {
                    if let Some(ref player) = round.players.get(name) {
                        for i in 0..player.cards.len() {
                            let (x, y) = card_texture_coordinates(&player.cards[i]);
                            draw_texture_ex(
                                self.cards,
                                screen_width() / 1.39 + (i as f32 * screen_width() / 30.),
                                screen_height() / 2.25,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x: x,
                                        y: y,
                                        w: CARD_TEXTURE_WIDTH,
                                        h: CARD_TEXTURE_HEIGHT,
                                    }),
                                    dest_size: Some(vec2(screen_width() / 32., screen_height() / 12.)),
                                    rotation: -0.97,
                                    ..Default::default()
                                },
                            );
                        }
                        
                        if let Some(stake) = player.stake {
                            let (x, y) = jeton_texture_coordinates(&stake);
                            draw_texture_ex(
                                self.jetons,
                                screen_width() / 1.295,
                                screen_height() / 1.9,
                                WHITE,
                                DrawTextureParams {
                                    source: Some(Rect {
                                        x,
                                        y,
                                        w: JETON_TEXTURE_WIDTH,
                                        h: JETON_TEXTURE_WIDTH
                                    }),
                                    dest_size: Some(vec2(screen_width() / 38.4, screen_height() / 21.6)),
                                    rotation: -0.97,
                                    ..Default::default()
                                },
                            );
                        }

                        draw_text_ex(
                            &status_text(&round, player),
                            screen_width() / 1.22,
                            screen_height() / 1.7,
                            TextParams {
                                font_size: (screen_height() + screen_width()) as u16 / 100,
                                font: self.font,
                                color: MAGENTA,
                                ..Default::default()
                            },
                        );
                    }
                }
            }
        }
        
        if let Some(time) = self.game_end_timer {
            draw_rectangle(
                screen_width() / 1.25,
                screen_height() / 10.,
                screen_width() / 2.,
                screen_height() / 16.,
                Color {r: 0., g: 0., b: 0., a: 0.8}
            );

            draw_text_ex(
                &format!("Game ending in {}", 100 - time),
                screen_width() / 1.23,
                screen_height() / 7.1,
                TextParams {
                    font_size: (screen_height() + screen_width()) as u16 / 100,
                    font: self.font,
                    color: RED,
                    ..Default::default()
                }
            );
        }

        // KEYCODES:
        if is_key_pressed(KeyCode::Enter) {
            if !self.started {
                println!("Enter key detected, starting game!");
                if let Some(ref mut stream) = stream {
                    let random_user_name = generate_user_name();
                    self.own_name = Some(random_user_name.clone());
                    if let Ok(_) =
                        send_message(stream, Message::RequestJoin(random_user_name.clone()))
                    {
                        self.started = true;
                        self.background = self.bg_game;
                    }
                } else {
                    eprintln!("Failed to join the game due to network error");
                }
            } else if !self.ready {
                if let Some(ref mut stream) = stream {
                    if let Ok(_) = send_message(stream, Message::UserReady) {
                        self.ready = true;
                    }
                } else {
                    eprintln!("Failed to set ready due to network error");
                }
            } else {
                if let Some(ref mut stream) = stream {
                    send_message(stream, Message::WantCard(true))?;
                }
            }
            play_sound(self.sound_card, PlaySoundParams {looped: false, volume: 0.02});
        }
        
        if is_key_pressed(KeyCode::Space) {
            if let Some(ref mut stream) = stream {
                send_message(stream, Message::WantCard(false))?;
            }
        }
        
        macro_rules! handle_stake_key {
            ($key: tt, $num: tt) => ({
                if let Some(ref round) = self.current_round {
                    if round.status == Status::SetStake {
                        if is_key_pressed(KeyCode::$key) {
                            if let Some(ref mut stream) = stream {
                                send_message(stream, Message::SetStake(Jeton::$num))?;
                            } else {
                                eprintln!("Network error");
                            }
                            play_sound(self.sound_jeton, PlaySoundParams {looped: false, volume: 0.02});
                        }
                    }
                }
            })
        }

        handle_stake_key!(Key1, One);
        handle_stake_key!(Key2, TwoPointFive);
        handle_stake_key!(Key3, Five);
        handle_stake_key!(Key4, Ten);
        handle_stake_key!(Key5, Twenty);
        handle_stake_key!(Key6, TwentyFive);
        handle_stake_key!(Key7, Hundred);
        handle_stake_key!(Key8, FiveHundred);

        if is_key_pressed(KeyCode::H) {
            if !self.show_controls {
                self.show_controls = true;
            } else {
                self.show_controls = false;
            }
        }

        next_frame().await;
        Ok(())
    }
}

struct MainLoop {
    client: NetworkClient,
    window: Window<'static>,
}

impl MainLoop {
    async fn new(address: &str) -> MainLoop {
        MainLoop {
            client: NetworkClient::new(address).expect("Failed to set up networking"),
            window: Window::new().await.unwrap(),
        }
    }

    async fn exec(&mut self) {
        // println!("Please enter your name:");
        // let mut name = String::new();
        // std::io::stdin().read_line(&mut name);

        loop {
            //println!("Loop");
            match self.client.process_event().await {
                Ok((messages, stream)) => {
                    if !messages.is_empty() {
                        for message in messages {
                            if let Err(e) =
                                self.window.process_event(Some(message), Some(stream)).await
                            {
                                eprintln!("Error in the gui: {}", e);
                            }
                        }
                    } else {
                        if let Err(e) = self.window.process_event(None, Some(stream)).await {
                            eprintln!("Error in the gui: {}", e);
                        }
                    }
                }
                Err(e) => {
                    eprintln!("The network layer had an error: {}", e);
                    if let Err(e) = self.window.process_event(None, None).await {
                        eprintln!("The gui had an error: {}", e);
                    }
                }
            }
        }
    }
}

#[macroquad::main(conf)]
async fn main() {
    let mut eventloop = MainLoop::new("localhost:1337").await;
    eventloop.exec().await;
}

fn conf() -> Conf {
    Conf {
        window_title: String::from("Rusty Blackjack"),
        window_width: 1280,
        window_height: 720,
        fullscreen: false,
        ..Default::default()
    }
}
