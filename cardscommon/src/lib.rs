#[macro_use]
extern crate serde;

use std::io::Write;
use std::net::TcpStream;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum BlackJackError {
    GameAlreadyRunning,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Move {}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Card {
    ClubsAce,
    ClubsTwo,
    ClubsThree,
    ClubsFour,
    ClubsFive,
    ClubsSix,
    ClubsSeven,
    ClubsEight,
    ClubsNine,
    ClubsTen,
    ClubsJack,
    ClubsQueen,
    ClubsKing,
    DiamondsAce,
    DiamondsTwo,
    DiamondsThree,
    DiamondsFour,
    DiamondsFive,
    DiamondsSix,
    DiamondsSeven,
    DiamondsEight,
    DiamondsNine,
    DiamondsTen,
    DiamondsJack,
    DiamondsQueen,
    DiamondsKing,
    HeartsAce,
    HeartsTwo,
    HeartsThree,
    HeartsFour,
    HeartsFive,
    HeartsSix,
    HeartsSeven,
    HeartsEight,
    HeartsNine,
    HeartsTen,
    HeartsJack,
    HeartsQueen,
    HeartsKing,
    SpadesAce,
    SpadesTwo,
    SpadesThree,
    SpadesFour,
    SpadesFive,
    SpadesSix,
    SpadesSeven,
    SpadesEight,
    SpadesNine,
    SpadesTen,
    SpadesJack,
    SpadesQueen,
    SpadesKing,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Jeton {
    One,
    TwoPointFive,
    Five,
    Ten,
    Twenty,
    TwentyFive,
    Hundred,
    FiveHundred
}

impl Card {
    pub fn value(&self) -> i32 {
        match self {
            Card::ClubsAce => 11,
            Card::ClubsTwo => 2,
            Card::ClubsThree => 3,
            Card::ClubsFour => 4,
            Card::ClubsFive => 5,
            Card::ClubsSix => 6,
            Card::ClubsSeven => 7,
            Card::ClubsEight => 8,
            Card::ClubsNine => 9,
            Card::ClubsTen => 10,
            Card::ClubsJack => 10,
            Card::ClubsQueen => 10,
            Card::ClubsKing => 10,
            Card::DiamondsAce => 11,
            Card::DiamondsTwo => 2,
            Card::DiamondsThree => 3,
            Card::DiamondsFour => 4,
            Card::DiamondsFive => 5,
            Card::DiamondsSix => 6,
            Card::DiamondsSeven => 7,
            Card::DiamondsEight => 8,
            Card::DiamondsNine => 9,
            Card::DiamondsTen => 10,
            Card::DiamondsJack => 10,
            Card::DiamondsQueen => 10,
            Card::DiamondsKing => 10,
            Card::HeartsAce => 11,
            Card::HeartsTwo => 2,
            Card::HeartsThree => 3,
            Card::HeartsFour => 4,
            Card::HeartsFive => 5,
            Card::HeartsSix => 6,
            Card::HeartsSeven => 7,
            Card::HeartsEight => 8,
            Card::HeartsNine => 9,
            Card::HeartsTen => 10,
            Card::HeartsJack => 10,
            Card::HeartsQueen => 10,
            Card::HeartsKing => 10,
            Card::SpadesAce => 11,
            Card::SpadesTwo => 2,
            Card::SpadesThree => 3,
            Card::SpadesFour => 4,
            Card::SpadesFive => 5,
            Card::SpadesSix => 6,
            Card::SpadesSeven => 7,
            Card::SpadesEight => 8,
            Card::SpadesNine => 9,
            Card::SpadesTen => 10,
            Card::SpadesJack => 10,
            Card::SpadesQueen => 10,
            Card::SpadesKing => 10,
        }
    }
}

pub fn cards_value(cards: &[Card]) -> i32 {
    let mut values: Vec<i32> = cards.iter().map(|c| c.value()).collect();
    //println!("Values: {:?}", values);
    //println!("Cards: {:?}", &cards);

    let mut value: i32 = values.iter().sum();
    //println!("base value {}", value);

    if value > 21 {
        for value in &mut values {
            if *value == 11 {
                *value = 1;
            }
        }

        value = values.iter().sum();
    }

    //println!("after ace handling {}", value);
    
    return value;
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum ProfitDistribution {
    Win,
    Loss,
    Tie,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Message {
    /// For debug purposes
    Hello(String),
    /// Request joining a game
    RequestJoin(String),
    /// An error occured
    Error(BlackJackError),
    /// The server signals the client that it has joined
    Joined,
    GameFull,
    /// An other user has joined
    UserJoined(String),
    UserReady,
    GameStarted,
    SetStake(Jeton),
    UserSetStake(String, Jeton),     // name, stake
    DealCards(String, Vec<Card>), // name, cards
    ShowDealerCard(Card),
    WantCard(bool),
    Over21(String), // name
    ProfitDistribution(String, ProfitDistribution), // name, profit distribution
    GameEnded,
}

pub fn send_message(stream: &mut TcpStream, message: Message) -> std::io::Result<()> {
    println!(">>> {:?}", message);
    writeln!(stream, "{}", serde_json::to_string(&message)?)
}
