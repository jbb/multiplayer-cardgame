use std::collections::HashMap;
use std::error::Error;
use std::io::BufRead;
use std::io::BufReader;
use std::net::SocketAddr;
use std::net::TcpListener;
use std::net::TcpStream;
use std::time::Duration;

use rand::seq::SliceRandom;

use cardscommon::*;

#[derive(Debug)]
struct PlayerState {
    nick: String,
    ready: bool,
    stake: Option<Jeton>,
    cards: Vec<Card>,
    round_over: bool,
}

impl PlayerState {
    fn new(nick: String) -> PlayerState {
        PlayerState {
            nick,
            ready: false,
            stake: None,
            cards: Vec::new(),
            round_over: false,
        }
    }
}

struct Game {
    players: HashMap<*mut TcpStream, PlayerState>,
    card_stack: Vec<Card>,
    dealer_cards: Vec<Card>,
}

impl Game {
    fn new() -> Game {
        let mut stack = vec![
            Card::ClubsAce,
            Card::ClubsTwo,
            Card::ClubsThree,
            Card::ClubsFour,
            Card::ClubsFive,
            Card::ClubsSix,
            Card::ClubsSeven,
            Card::ClubsEight,
            Card::ClubsNine,
            Card::ClubsTen,
            Card::ClubsJack,
            Card::ClubsQueen,
            Card::ClubsKing,
            Card::DiamondsAce,
            Card::DiamondsTwo,
            Card::DiamondsThree,
            Card::DiamondsFour,
            Card::DiamondsFive,
            Card::DiamondsSix,
            Card::DiamondsSeven,
            Card::DiamondsEight,
            Card::DiamondsNine,
            Card::DiamondsTen,
            Card::DiamondsJack,
            Card::DiamondsQueen,
            Card::DiamondsKing,
            Card::HeartsAce,
            Card::HeartsTwo,
            Card::HeartsThree,
            Card::HeartsFour,
            Card::HeartsFive,
            Card::HeartsSix,
            Card::HeartsSeven,
            Card::HeartsEight,
            Card::HeartsNine,
            Card::HeartsTen,
            Card::HeartsJack,
            Card::HeartsQueen,
            Card::HeartsKing,
            Card::SpadesAce,
            Card::SpadesTwo,
            Card::SpadesThree,
            Card::SpadesFour,
            Card::SpadesFive,
            Card::SpadesSix,
            Card::SpadesSeven,
            Card::SpadesEight,
            Card::SpadesNine,
            Card::SpadesTen,
            Card::SpadesJack,
            Card::SpadesQueen,
            Card::SpadesKing,
        ];

        stack.shuffle(&mut rand::thread_rng());

        Game {
            players: HashMap::new(),
            card_stack: stack,
            dealer_cards: Vec::new(),
        }
    }

    fn get_player(&mut self, stream: &mut TcpStream) -> Option<&mut PlayerState> {
        self.players.get_mut(&(stream as *mut TcpStream))
    }

    fn handle_message(
        &mut self,
        message: Message,
        stream: &mut TcpStream,
        global_queue: &mut Vec<Message>,
    ) -> Result<(), Box<dyn Error>> {
        match message {
            Message::Hello(text) => {
                println!("Client says: {}", text);
                Ok(())
            }
            Message::RequestJoin(name) => {
                if self.players.len() > 4 {
                    global_queue.push(Message::GameFull);
                    return Ok(())
                }
                println!("{} wants to join", name);
                send_message(stream, Message::Joined)?;
                self.players.insert(stream, PlayerState::new(name.clone()));
                global_queue.push(Message::UserJoined(name));
                Ok(())
            }
            Message::UserReady => {
                if let Some(mut player) = self.get_player(stream) {
                    player.ready = true;
                } else {
                    eprintln!("User was ready before joining, this is a client bug");
                }

                // When all players are ready, start the game for all of them.
                if self.players.keys().all(|key| {
                    self.players
                        .get(key)
                        .expect("This can't happen because we are only passing keys from the map")
                        .ready
                }) {
                    global_queue.push(Message::GameStarted);
                    // The players should now set their stakes
                }
                Ok(())
            }
            Message::SetStake(stake) => {
                if let Some(mut player) = self.get_player(stream) {
                    player.stake = Some(stake);
                    global_queue.push(Message::UserSetStake(player.nick.clone(), stake));
                } else {
                    eprintln!("User set stake before joining, this is a client bug");
                }

                if self.players.keys().all(|key| {
                    self.players
                        .get(key)
                        .expect("This can't happen because we are only passing keys from the map")
                        .stake
                        .is_some()
                }) {
                    for (stream, player) in &mut self.players {
                        if !stream.is_null() {
                            let mut cards =
                                vec![self.card_stack.remove(0), self.card_stack.remove(0)];
                            global_queue
                                .push(Message::DealCards(player.nick.clone(), cards.clone()));
                            player.cards.append(&mut cards);
                        }
                    }

                    self.dealer_cards = vec![self.card_stack.remove(0), self.card_stack.remove(0)];
                    global_queue.push(Message::ShowDealerCard(self.dealer_cards[0].clone()));
                    // We just set it to a value
                }
                Ok(())
            }
            Message::WantCard(want_card) => {
                if want_card {
                    let name = self.get_player(stream).unwrap().nick.clone();
                    let card = self.card_stack.remove(0);
                    global_queue.push(Message::DealCards(name, vec![card]));

                    let player = self
                        .get_player(stream)
                        .expect("Player should exist in the game");
                    player.cards.push(card);
                } else {
                    let player = self
                        .get_player(stream)
                        .expect("Player should exist in the game");
                    player.round_over = true;
                }

                if let Some(ref mut player) = self.get_player(stream) {
                    let value = cards_value(&player.cards);
                    if value > 21 {
                        global_queue.push(Message::Over21(player.nick.clone()));
                        player.round_over = true;
                    }
                }

                if self.players.keys().all(|key| {
                    self.players
                        .get(key)
                        .expect("This can't happen because we are only passing keys from the map")
                        .round_over
                }) {
                    // zweite Dealer-Karte aufdecken
                    global_queue.push(Message::ShowDealerCard(self.dealer_cards[1]));

                    // dealer macht seine aktionen
                    let mut dealer_cards_value: i32 = cards_value(&self.dealer_cards);

                    println!("Dealer vor'm ziehen {}", dealer_cards_value);
                    while dealer_cards_value < 17 {
                        let card = self.card_stack.remove(0);
                        self.dealer_cards.push(card.clone());
                        global_queue.push(Message::ShowDealerCard(card));

                        std::thread::sleep(Duration::from_secs(1));

                        // recalculate value
                        dealer_cards_value = cards_value(&self.dealer_cards);
                    }

                    println!("Dealer nachm ziehen: {}", dealer_cards_value);

                    // profit distribution
                    for (_stream, player) in &self.players {
                        //println!("{:?}", player);
                        let player_cards_value: i32 = cards_value(&player.cards);
                        if player_cards_value > 21 && dealer_cards_value > 21 {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Tie));
                            continue;
                        }
                        if dealer_cards_value > 21 {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Win));
                            continue;
                        }
                        if player_cards_value > 21 {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Loss));
                            continue;
                        }

                        //println!("Player cards value: {}", player_cards_value);
                        if player_cards_value > dealer_cards_value {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Win));
                            continue;
                        } else if player_cards_value == dealer_cards_value {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Tie));
                            continue;
                        } else if player_cards_value < dealer_cards_value {
                            global_queue.push(Message::ProfitDistribution(player.nick.clone(), ProfitDistribution::Loss));
                            continue;
                        }
                    }

                    // Clean up round data
                    global_queue.push(Message::GameEnded);
                    for player in &mut self.players.values_mut() {
                        player.ready = false;
                        player.stake = None;
                        player.cards.clear();
                        player.round_over = false;
                    }
                }

                Ok(())
            }
            _ => {
                panic!("Unsupported message received");
            }
        }
    }
}

struct NetworkServer {
    listener: TcpListener,
    store: Game,
    connections: Vec<TcpStream>,
    // messages that should be sent to all players
    outgoing_messages: Vec<Message>,
}

impl NetworkServer {
    fn log(stream: &TcpStream) {
        stream
            .peer_addr()
            .and_then(|addr: SocketAddr| {
                println!("<<< incoming from {}", addr.ip());
                Ok(())
            })
            .or_else(|_e: std::io::Error| {
                println!("Request from unknown ip address");
                Err(())
            })
            .ok();
    }

    fn new(address: &str) -> std::io::Result<NetworkServer> {
        Ok(NetworkServer {
            listener: TcpListener::bind(address)?,
            store: Game::new(),
            connections: Vec::new(),
            outgoing_messages: Vec::new(),
        })
    }

    fn listen(&mut self) -> std::io::Result<()> {
        // accept connections and process them serially
        self.listener.set_nonblocking(true)?;
        loop {
            // Dispatch global messages
            let mut connection_to_drop: Option<usize> = None;
            let mut i: usize = 0;
            for message in self.outgoing_messages.drain(..) {
                for mut connection in &mut self.connections {
                    if let Err(e) = send_message(&mut connection, message.clone()) {
                        eprintln!("Failed to send message to all players: {}", e);
                        eprintln!("Probably the client already disconnected");
                        eprintln!("Dropping connection");

                        connection_to_drop = Some(i);
                    }
                    i += 1;
                }
            }

            if let Some(index) = connection_to_drop {
                self.connections.remove(index);
            }

            if let Ok((mut stream, _addr)) = self.listener.accept() {
                NetworkServer::log(&stream);

                // Propagate player joins that happend before the new player was connected
                for nick in self.store.players.values().map(|p| p.nick.clone()) {
                    send_message(&mut stream, Message::UserJoined(nick))?;
                }

                self.connections.push(stream);
            }

            for mut connection in &mut self.connections {
                let mut line = String::new();
                connection.set_read_timeout(Some(Duration::from_millis(600)))?;
                if let Ok(_) = BufReader::new(&mut connection).read_line(&mut line) {
                    if !line.is_empty() {
                        match serde_json::from_str(&line) {
                            Ok(message) => {
                                println!("<<< {:?}", message);
                                if let Err(e) = self.store.handle_message(
                                    message,
                                    connection,
                                    &mut self.outgoing_messages,
                                ) {
                                    eprintln!("Error occured in message handler: {}", e);
                                    eprintln!("Maybe the client suddenly disconnected?");
                                }
                            }
                            Err(e) => {
                                eprintln!("The message from the client was invalid: {}", e);
                            }
                        }
                    }
                }
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut server = NetworkServer::new("0.0.0.0:1337")?;
    server.listen()
}
